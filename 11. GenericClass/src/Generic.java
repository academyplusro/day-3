//Generic indicates that the class is of generic type
public class Generic <Employee>{  
	
	//an object of type Employee is declared
	Employee ob;     
	//constructor
	Generic(Employee o){  
	    ob = o;
	}
	public Employee getOb(){
	 
		return ob;
	}

	
} 
	
