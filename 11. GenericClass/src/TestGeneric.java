public class TestGeneric {

	public static void main(String[] args) {
		
		{
			//instance of Strings type Generic Class.
			Generic <String> sob = new Generic<>("My name is Samuel Jackson. ");
			Generic <String> sob1 = new Generic<>("I work as cashier at Plaza hotel. ");
			  String str = sob.getOb();
			  String str1 = sob1.getOb();
			  System.out.println(str);
			  System.out.println(str1);
			//instance of Integer type Generic Class.
		    Generic < Integer> iob = new Generic<>(2000);     
			  int  x = iob.getOb();
			  System.out.println("My salary is valued somewhere around "+ x + " dollars. ");
		  
		}
	
	}
}

