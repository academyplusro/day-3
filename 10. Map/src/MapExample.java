import java.util.*;

public class MapExample {

	public static void main(String[] args) {
		        Map employees = new HashMap();
		        // Add some employees.
		        employees.put("Bartenders", 5);
		        employees.put("Receptionists", 3);
                employees.put("Porters", 8);
		 		employees.put("Cashiers", 10);
	
		         System.out.println("Total type of hotel employees: " + employees.size());
		 
	             // Iterate over all employees, using the keySet method.
	             for(Object key: employees.keySet())
	 
	             System.out.println(key + " - " + employees.get(key));
	 
	             System.out.println();
	 
	          
	             String searchKey = "Cashiers";
		         if(employees.containsKey(searchKey))
	                 System.out.println("Found total " + employees.get(searchKey) + " "
		                                + searchKey + " employees!\n");    
		         else {
		        	 System.out.println("Keyword not found!");
		        	 System.out.println("");
		         }
		        	
		
		         // Clear all values.
		         employees.clear();
		 
		         // Equals to zero.
	             System.out.println("After clear operation, size is now : " + employees.size());
		         

		  }
		 
 }
		 		 