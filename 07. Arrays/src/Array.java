
public class Array {

	public static void main(String[] args) {
		
		
	    	//declare an array of Strings
		String [] receptionists;
		
		// allocates memory for 5 Strings
		receptionists = new String[5];
		receptionists [0] = "John Snow";
		receptionists [1] = "William Copper";
		receptionists [2] = "Patricia Johnson";
		receptionists [3] = "Kate Anderson";
		receptionists [4] = "Dustin Rhodes";
		
		//declare an array of Integers
		int floor[] ;
		//allocates memory for 5 integers
		floor = new int[5];
		floor [0] = 1;
		floor [1] = 2;
		floor [2] = 3;
		floor [3] = 4; 
		floor [4] = 5;

		System.out.println("The receptionist on the " + floor[0]+ " -st floor is: "
		                + receptionists[0]);
		System.out.println("The receptionist on the " + floor[1]+ " -nd  floor is: "
		                + receptionists[1]);
		System.out.println("The receptionist on the " + floor[2]+ " -rd  floor is: "
		                + receptionists[2]);
		System.out.println("The receptionist on the " + floor[3]+ " -th  floor is: "
		                + receptionists[3]);
		System.out.println("The receptionist on the " + floor[4]+ " -th  floor is: "
		                + receptionists[4]);
		 System.out.println("");
		 
		//multidimensional array 2D
			String[][] names = {
		            {"Mr. ", "Ms. ", "Mrs. "},
		            {"Smith Riley", "Catherine Jade" ,"Emma Hudson"}
		        };
		        // Mr.SmithRiley
		        System.out.print(names[0][0] + names[1][0]+ " and ");
		        // Mrs.Catherine 
		        System.out.println(names[0][2] + names[1][1]+ " are the owners of the Plaza Hotel");
		        System.out.println("");
		
		/*declare an array of char elements, spelling the words "'B','E',D', 'O', 'N', 'A', 
		 * 'L','D', 'A', 'T', 'E'.
		 * Use the System.arraycopy method to copy a subsequence of array 
		 * components into a second array:
		 */
		char[] copyFrom = { 'B', 'E', 'D', 'O', 'N', 'A', 'L',
			    'D', 'N', 'A', 'T', 'E' };
        char[] copyTo = new char[6];

        System.arraycopy(copyFrom, 2, copyTo, 0, 6);
        System.out.println("The hotel's security is provided by: " +new String(copyTo));
        
      
	}

}
