//Generic Type with more than one parameter 
public class GenTypes <T1,T2>{
		T1 name; //An object of type T1
		T2 value;//An object of type T2

		//constructor
		 GenTypes(T1 o1,T2 o2) {
		 this.name = o1;
		 this.value = o2;
		}
		 //getter methods
		 public T1 getName(){
		  return name;
		}
	
		 public T2 getValue(){
		  return value;
		}

}