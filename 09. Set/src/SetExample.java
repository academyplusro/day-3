import java.util.*;

public class SetExample {

	public static void main(String[] args) {
		
		// We create a new, empty set
        Set<String> myEmployeesSet1 = new HashSet<String>();
        // We add a few elements
        myEmployeesSet1.add("Samuel Smith");
		myEmployeesSet1.add("Cartner Caragher");
		myEmployeesSet1.add("Anthony Johnson");
		myEmployeesSet1.add("Bill Williams");
		myEmployeesSet1.add("Bill Williams");
		// Print the elements of the Set
		System.out.println("myEmployeesSet1: " + myEmployeesSet1);

		//Create a list and add some elements
        List<String> list = new ArrayList<String>();
		list.add("Astrid Conner");
		list.add("Christopher Adams");
		list.add("Antoine Griezmann");
		list.add("Adam Sandler");
		list.add("Bailey Aidan");
		list.add("Carl Edwin");
		list.add("Carl Edwin");
		// Now create the set using the appropriate constructor
		Set<String> myEmployeesSet2 = new HashSet<String>(list);
		// Print the elements of the list an the the set
		System.out.println("list: " + list);
		System.out.println("myEmployeesSet2: " + myEmployeesSet2);
		
		// Compare the two sets
		System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + myEmployeesSet1.equals(myEmployeesSet2));
		// Now we will remove one element from myEmployeesSet2 and compare again
		myEmployeesSet2.remove("Adam Sandler");
        System.out.println("myEmployeesSet2: " + myEmployeesSet2);
        System.out.println("myEmployeesSet1 matches myEmployeesSet2: " + myEmployeesSet1.equals(myEmployeesSet2));
        
        // Lets check if our sets contain all the elements of the list
        System.out.println("myEmployeesSet1 contains all the elements: " + myEmployeesSet1.containsAll(list));
        System.out.println("myEmployeesSet2 contains all the elements: " + myEmployeesSet2.containsAll(list));

        // Use of Iterator in Set
        Iterator<String> iterator = myEmployeesSet1.iterator();
        while (iterator.hasNext()) {
            System.out.println("Iterator loop: " + iterator.next());
        }
        

        // Clearing all the elements from set1
        myEmployeesSet1.clear();
        System.out.println("myEmployeesSet1 is Empty: " + myEmployeesSet1.isEmpty());

        // Checking the number of elements
        System.out.println("myEmployeesSet1 has: " + myEmployeesSet1.size() + " Elements");
        System.out.println("myEmployeesSet2 has: " + myEmployeesSet2.size() + " Elements");

        // Creating an Array with the contents of the set
        Object[] array = myEmployeesSet2.toArray(new String[myEmployeesSet2.size()]);
        System.out.println("The array:" + Arrays.toString(array));


	}

}
